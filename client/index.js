import '@babel/polyfill'
import {h,render} from '../shared/modules'
import {Shell} from '../shared/components'
import {InitProps, root} from '../shared/config'
render(h(Shell, InitProps), root())