import {router} from './'
import {handler} from './tx'
import {filterEsModule} from './utils'

import * as Schemes from '../schemes'
import * as PreloadData from '../data'


export const init = () => {


    filterEsModule(PreloadData)
        .forEach(cid => {

            if (['pages', 'products'].includes(cid))
                handler({cid}, PreloadData[cid])
            console.log({cid})

        })

    filterEsModule(Schemes)
        .forEach(id => handler({cid: 'schemes'}, {id, ...Schemes[id]}))

    return IS_CLIENT ? router.start() : false
}
