import {resolve} from 'path'

// export const IS_DEV = process.env.NODE_ENV === 'development'
export const IS_CLIENT = true
export const IS_SERVER = !IS_CLIENT

export const DEV_PORT = 8888


const ROOT = process.cwd()

export const
    output = resolve(ROOT, 'static'),
    shared = resolve(ROOT, 'shared'),
    assets = resolve(output, 'assets'),
    dll = resolve(assets, 'dll', '[name].json'),
    vendor = resolve(assets, 'dll', 'vendor.json'),
    svg = resolve(output, 'svg'),
    exclude = resolve(ROOT, 'node_modules')


//2m = 120 000 ms
//5m = 300 000 ms
export const
    CACHE_LIFE_TIME = 600000,
    EMPTY = Object.create(null),
    root = () => document.body,
    InitProps = Object.create(null),
    dbName = 'app.db',
    dbID = 'id',
    dbCollOpts = {unique: [dbID]}